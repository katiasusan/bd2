package com.example.alexistrujillo.bd;

import android.provider.BaseColumns;


//Almacena los nombres de la bd, tablas y campos de la tabla
public class FeedReaderContract {

    public FeedReaderContract(){

    }

    public static abstract class FeedEntry implements BaseColumns {

        public static final String BOOK_ID = "id";
        public static final String BOOK_NAME = "name";
        public static final String BOOK_AUTHOR = "author";
        public static final String BOOK_DESCRIPTION = "description";
        public static final String BOOK_IMAGE = "image";
        public static final String DATABASE_NAME = "book_database";
        public static final String TABLE_NAME = "book";

    }
}
