package com.example.alexistrujillo.bd;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.example.alexistrujillo.bd.FeedReaderContract.FeedEntry;

public class    DatabaseHelper extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 1;
    private static final String TAG = "DatabaseHelper;";

    //QUERY para crear tabla book
    private static final String CREATE_QUERY = "CREATE TABLE "+ FeedEntry.TABLE_NAME+" ( "+
            FeedEntry.BOOK_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            FeedEntry.BOOK_NAME + " TEXT, "+
            FeedEntry.BOOK_AUTHOR + " TEXT, "+
            FeedEntry.BOOK_IMAGE + " TEXT, "+
            FeedEntry.BOOK_DESCRIPTION + " TEXT );";


    //QUERY para elminar tabla
    private static final String DROP_QUERY = "DROP TABLE IF EXISTS "+ FeedEntry.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, FeedEntry.DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_QUERY);
        onCreate(db);
    }

}
