package com.example.alexistrujillo.bd;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.alexistrujillo.bd.FeedReaderContract.FeedEntry;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mBookDatabase;
    private EditText nombre;
    private EditText autor;
    private EditText descripcion;
    private ImageView foto;
    private Button agregar;
    private Button eliminar;
    private Button editar;
    private Button avanzar;
    private Button retroceder;
    private Button primero;
    private Button ultimo;
    private Button limpiar;

    private String URL;
    private Download download;

    private Cursor fila;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        nombre= (EditText)findViewById(R.id.editname);
        autor= (EditText)findViewById(R.id.editauthor);
        descripcion= (EditText)findViewById(R.id.editdesripcion);
        agregar= (Button) findViewById(R.id.agregar);
        eliminar= (Button)findViewById(R.id.eliminar);
        editar= (Button) findViewById(R.id.editar);
        avanzar= (Button)findViewById(R.id.avanzar);
        retroceder= (Button) findViewById(R.id.retroceder);
        primero= (Button)findViewById(R.id.primero);
        ultimo =(Button)findViewById(R.id.ultimo);
        limpiar =(Button)findViewById(R.id.limpiar);
        foto = (ImageView) findViewById(R.id.hcorpo);

        mDatabaseHelper = new DatabaseHelper(this);//DatabaseHelper hereda de SQLiteOpenHelper
        //mBookDatabase = mDatabaseHelper.getWritableDatabase();//SQLiteDatabase

        //readFromInternal();

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseHelper dbh= new DatabaseHelper(getApplicationContext());
                SQLiteDatabase bd =dbh.getWritableDatabase();

                String nombrelibro=nombre.getText().toString();
                String nombreautor=autor.getText().toString();
                String descripcionlibro=descripcion.getText().toString();

                ContentValues cv1=new ContentValues();
                cv1.put("name",nombrelibro);
                cv1.put("author",nombreautor);
                cv1.put("description",descripcionlibro);

                bd.insert("book",null,cv1);
                bd.close();
                nombre.setText("");
                autor.setText("");
                descripcion.setText("");
                Toast.makeText(getApplicationContext(),"libro agregado",Toast.LENGTH_LONG).show();
            }

        });

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper dbh = new DatabaseHelper(getApplicationContext());
                SQLiteDatabase bd = dbh.getWritableDatabase();

               // fila=bd.rawQuery("select * from book",null);

                String id= fila.getString(0);
                int r=fila.getPosition();

                String nombrelibro = nombre.getText().toString();
                String autorlibro= autor.getText().toString();
                String descripcionlibro = descripcion.getText().toString();

                ContentValues cv = new ContentValues();

                cv.put("name",nombrelibro);
                cv.put("author",autorlibro);
               // cv.put("image","null");
                cv.put("description",descripcionlibro);

                int cant = bd.update("book",cv,"id="+ id,null);
              //  bd.execSQL("SELECT * FROM book WHERE id="+id);
                fila = bd.rawQuery("select * from book order by id asc",null);
                boolean t= fila.moveToPosition(r);
                //Log.i("poscicion",t+"");

                bd.close();
                if (cant == 1)
                    Toast.makeText(getApplicationContext(),"ModificaciÃ³n realizada",Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getApplicationContext(), "No se encuentra", Toast.LENGTH_LONG).show();

            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper dbh = new DatabaseHelper(getApplicationContext());
                SQLiteDatabase bd = dbh.getWritableDatabase();
               // String nombrelibro =nombre.getText().toString();

                String id=fila.getString(0);
                int cant = bd.delete("book", "id=" +id,null);
                //Toast.makeText(getApplicationContext(),"cant_elim= "+cant,Toast.LENGTH_LONG).show();
                bd.close();
                nombre.setText("");
                autor.setText("");
                descripcion.setText("");
                if (cant == 1)
                    Toast.makeText(getApplicationContext(),"Borrado",Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getApplicationContext(),"No existe",Toast.LENGTH_LONG).show();
            }
        });


        primero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper dbh = new DatabaseHelper(getApplicationContext());
                SQLiteDatabase bd = dbh.getWritableDatabase();

                fila = bd.rawQuery("select * from book order by id asc",null);
                if (fila.moveToFirst()) {
                    nombre.setText(fila.getString(1));
                    autor.setText(fila.getString(2));
                    descripcion.setText(fila.getString(4));

                    URL = "http:"+fila.getString(3);
                    download = new Download();
                    download.execute(URL);

                } else
                    Toast.makeText(getApplicationContext(),"No hay registrados",Toast.LENGTH_LONG).show();
                bd.close();
            }
        });

        ultimo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper dbh=new DatabaseHelper(getApplicationContext());
                SQLiteDatabase bd=dbh.getWritableDatabase();
                fila=bd.rawQuery("select * from book order by id asc",null);
                if (fila.moveToLast()) {
                    nombre.setText(fila.getString(1));
                    autor.setText(fila.getString(2));
                    descripcion.setText(fila.getString(4));

                    URL = "http:"+fila.getString(3);
                    download = new Download();
                    download.execute(URL);

                } else
                    Toast.makeText(getApplicationContext(),"No hay registros",Toast.LENGTH_LONG).show();
                bd.close();
            }
        });

        retroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper dbh=new DatabaseHelper(getApplicationContext());
                SQLiteDatabase bd=dbh.getWritableDatabase();

              try {
                    if (!fila.isFirst()) {
                        fila.moveToPrevious();





                        nombre.setText(fila.getString(1));
                        autor.setText(fila.getString(2));
                        descripcion.setText(fila.getString(4));

                        URL = "http:"+fila.getString(3);
                        download = new Download();
                        download.execute(URL);

                    } else
                        Toast.makeText(getApplicationContext(),"Inicio de la tabla",Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
                bd.close();
            }
        });

        avanzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper dbh=new DatabaseHelper(getApplicationContext());
                SQLiteDatabase bd=dbh.getWritableDatabase();
                try {
                    if (!fila.isLast()) {


                        fila.moveToNext();


                        nombre.setText(fila.getString(1));
                        autor.setText(fila.getString(2));
                        descripcion.setText(fila.getString(4));

                        URL = "http:"+fila.getString(3);
                        download = new Download();
                        download.execute(URL);


                    } else
                        Toast.makeText(getApplicationContext(),"Llegó al final",Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
                bd.close();
            }
        });

        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nombre.setText("");
                autor.setText("");
                descripcion.setText("");
                foto.setImageResource(R.drawable.ic_favorite_black_24dp);
            }
        });


        //Se lee de la memoria interna

        //Esta llamada se debe hacer solo una vez, en caso
        //Agregar verificacion si la tabla esta vacia con cursores,
        //si esta vacia se lee los datos de la memoria interna
        //si no se deja como esta
        //Si se corren mas de una vez el programa y se siguen leyendo los mismos archivos hay conflictos de id para los libros
       //readFromInternal();

        //mDatabaseHelper.close();
        //mBookDatabase.close();

    }



    public Bitmap descargarImagen(String imageHttpAddress) {
        java.net.URL imageUrl;
        Bitmap img = null;
        try {
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            img = BitmapFactory.decodeStream(conn.getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return img;
    }

    private class Download extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String url = params[0];
            Bitmap imagen = descargarImagen(url);
            return imagen;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);

            foto.setImageBitmap(result);
            //Toast.makeText(MainActivity.this, "Descarga finalizada!", Toast.LENGTH_SHORT).show();
        }

    }

    //New

    public void readFromInternal() {
        int i= 0;
        String id, name, author, description, image;
        String args[];
        ContentValues cv = new ContentValues();
        mBookDatabase = mDatabaseHelper.getWritableDatabase();//SQLiteDatabase
        try {

            InputStream fraw = getResources().openRawResource(R.raw.data);
            BufferedReader br = new BufferedReader(new InputStreamReader(fraw));

            String line;
            while ((line = br.readLine()) != null) {
                //Se lee linea a linea
                //Log.d("ARCHIVO: ", line);
                //Los srparadores usados son #
                //El formato es : id#titulo#author%descripcion#urlimagen
                args = line.split("#");
                //Se obtiene cada campo por separado
                id = args[0];
                name = args[1];
                author = args[2];
                description = args[3];
                image = args[4];

                cv.put(FeedEntry.BOOK_ID, id);
                cv.put(FeedEntry.BOOK_NAME, name);
                cv.put(FeedEntry.BOOK_AUTHOR,author);
                cv.put(FeedEntry.BOOK_IMAGE, image);
                cv.put(FeedEntry.BOOK_DESCRIPTION, description);

                //Se ingresan los campos en la bd
               long k = mBookDatabase.insert(FeedEntry.TABLE_NAME, null, cv);
                //Log.d("ESCRIBIENDO EN BD:", ""+k);


                i++;
            }
            //Log.d("ARCHIVO: ", ""+i);
            br.close();
        }
        catch (FileNotFoundException ex) {
            Log.e("FILES","ERROR AL ABRIR FICHERO");
            ex.printStackTrace();
        }
        catch (IOError ex) {
            Log.e("FILES","ERROR AL LEER FICHERO");
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    //*************************
   /* public void readFromInternal() {
        int i= 0;
        String id, name, author, description, image;
        String args[];
        ContentValues cv = new ContentValues();
        try {

            //El archivo data se coloca en /data/data/ con el  Android device monitor en Tools
            String path = "/data/data/data.txt";


            File f = new File(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
            String line;
            while ((line = br.readLine()) != null) {
                //Se lee linea a linea
                //Log.d("ARCHIVO: ", line);
                //Los srparadores usados son #
                //El formato es : id#titulo#author%descripcion#urlimagen
                args = line.split("#");
                //Se obtiene cada campo por separado
                id = args[0];
                name = args[1];
                author = args[2];
                description = args[3];
                image = args[4];

                cv.put(FeedEntry.BOOK_ID, id);
                cv.put(FeedEntry.BOOK_NAME, name);
                cv.put(FeedEntry.BOOK_AUTHOR,author);
                cv.put(FeedEntry.BOOK_IMAGE, image);
                cv.put(FeedEntry.BOOK_DESCRIPTION, description);

                //Se ingresan los campos en la bd
                long k = mBookDatabase.insert(FeedEntry.TABLE_NAME, null, cv);
                //Log.d("ESCRIBIENDO EN BD:", ""+k);


                i++;
            }
            //Log.d("ARCHIVO: ", ""+i);
            br.close();
        }
        catch (FileNotFoundException ex) {
            Log.e("FILES","ERROR AL ABRIR FICHERO");
            ex.printStackTrace();
        }
        catch (IOError ex) {
            Log.e("FILES","ERROR AL LEER FICHERO");
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }*/
}
